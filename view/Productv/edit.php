<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';

$object = new App\admin\Products\Products();
$data = $object->view($_GET['id']);
?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Basic Product Add Form
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/Productv/update.php" method="POST">
                                    <div class="form-group">
                                        <label>Product Title</label>
                                        <input value="<?php echo $data['title']?>" name="title" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="category" class="form-control">
                                            <option><?php echo $data['category']?></option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Baby">Baby</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input value="<?php echo $data['description']?>" name="description" class="form-control" height="50">
<!--                                        <textarea  ></textarea>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Product Price$</label>
                                        <input value="<?php echo $data['price']?>" name="price" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Upload Image</label>
                                        <input value="<?php echo $data['image']?>" name="image" type="file"/>
                                    </div>

                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="view/Productv/index.php" class="btn btn-default" style="margin-top: 5px">Cancel</a>

                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>