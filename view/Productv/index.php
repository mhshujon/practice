<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';
if(!isset($_SESSION)){
    session_start();
}
?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div style="position: fixed; z-index: 111; right: 30px; top: 65px">
            <?php
            if(isset($_SESSION['msg'])) {
                echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                session_unset();
            }
            if(isset($_SESSION['delete'])) {
                echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                session_unset();
            }
            if(isset($_SESSION['update'])) {
                echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                session_unset();
            }
            ?>
        </div>

        <div class="row">
            <?php
            $object = new \App\admin\Products\Products();
            $products = $object->index();
            foreach ($products as $product){
            ?>
            <div class="col-md-3 col-sm-6">

                <span class="thumbnail">
                    <img src="https://s12.postimg.org/41uq0fc4d/item_2_180x200.png" alt="...">
                    <h4><?php echo $product['title']?></h4>
                    <div class="ratings">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </div>
                    <p><?php echo $product['description']?></p>
                    <hr class="line">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <p class="price"><?php echo $product['price']?></p>
                        </div>
                        <div class="col-md-6 col-sm-6">
                         <a href="view/Productv/view.php?id=<?php echo $product['id']?>">	<button class="btn btn-info right" > Details</button></a>
                        </div>

                    </div>
                </span>
            <!-- END PRODUCTS -->
            </div>
            <?php } ?>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>