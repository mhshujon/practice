<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 9/24/2017
 * Time: 11:33 PM
 */
namespace App;
use PDO;
use PDOException;
class Connection
{
    private $user = 'root';
    private $pass = '';
    protected $con;

    public function __construct()
    {
        try {
            $this->con = new PDO('mysql:host=localhost;dbname=daily-bazar-db', $this->user, $this->pass);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}