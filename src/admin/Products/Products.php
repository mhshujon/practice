<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 9/24/2017
 * Time: 11:39 PM
 */
namespace App\admin\Products;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDO;
use PDOException;
class Products extends Connection
{
    private $title;
    private $category;
    private $description;
    private $price;
    private $image;
    private $id;

    public function set($data = array()){
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('category', $data)) {
            $this->category = $data['category'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('price', $data)) {
            $this->price = $data['price'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `items-table` (`title`, `category`, `description`, `price`, `image`)
                                                                      VALUES(:title,:category,:description,:price,:image)");
            $result =  $stmt->execute(array(
                ':title' => $this->title,
                ':category' => $this->category,
                ':description' => $this->description,
                ':price' => $this->price,
                ':image' => $this->image
            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!';
                header('location:index.php');
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `items-table`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function view($id){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `items-table` WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function delete($id){
        try{
            $stmt = $this->con->prepare("DELETE FROM `items-table` WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                $_SESSION['delete'] = 'Data successfully Deleted !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `daily-bazar-db`.`items-table` SET `title` = :title, `category` = :category, `description` = :description, `price` = :price, `image` = :image WHERE `items-table`.`id` = :id;");

            $result =  $stmt->execute(array(
                ':title' => $this->title,
                ':category' => $this->category,
                ':description' => $this->description,
                ':price' => $this->price,
                ':image' => $this->image
            ));
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}